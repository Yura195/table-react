import { useEffect, useState } from "react";
import styles from "./Table.module.css";

const Table = () => {
  const [items, setItems] = useState([]);
  const categories = ["ID", "Name", "Age", "Phone"];
  useEffect(() => {
    fetch("http://localhost:3000/persons")
      .then((response) => response.json())
      .then((result) => {
        setItems(result);
      });
  }, []);

  return (
    <div className={styles.table}>
      <div className={styles.header}>
        {categories.map((category, index) => (
          <div className={`${styles.cell} ${styles.category}`} key={index}>
            {category}
          </div>
        ))}
      </div>
      {items.map((item, index) => (
        <div className={styles.row} key={index}>
          <div className={styles.cell}>{item.id}</div>
          <div className={styles.cell}>{item.name}</div>
          <div className={styles.cell}>{item.age}</div>
          <div className={styles.cell}>{item.phone}</div>
        </div>
      ))}
    </div>
  );
};

export default Table;
